package cat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.StringTokenizer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

class Cat implements Runnable {

    final String catRight = 
    "               /\\___/\\\n"+
    "               ( o   o )\n"+
    "               (  =^=  )\n"+
    "              (        )\n"+
    "             (         )\n"+
    "  (((((((((((          )\n";
    final String catLeft = 
    " /\\___/\\\n"+
    " ( o   o )\n"+
    " (  =^=  )\n"+
    " (        )\n"+
    " (         )\n"+
    " (          )))))))))))\n";
    final String catRightMeow = 
    "               /\\___/\\\n"+
    "               ( ^   ^ )\n"+
    "               (  =o=  )\n"+
    "              (        )\n"+
    "             (         )\n"+
    "  (((((((((((          )\n";
    final String catLeftMeow = 
    " /\\___/\\\n"+
    " ( ^   ^ )\n"+
    " (  =o=  )\n"+
    " (        )\n"+
    " (         )\n"+
    " (          )))))))))))\n";
    final int xLimit = 20;

    static boolean isRunning = true;
    volatile int position = 0;
    volatile boolean isRight = true;
    volatile int meowTime = 0;

    public void shutDown(){isRunning = false;}

    public synchronized void renderCat(){

        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch(Exception e) {}

        String cat;
        if(isRight && meowTime > 0) {
            cat = catRightMeow;
        } else if(isRight && meowTime <= 0) {
            cat = catRight;
        } else if(!isRight && meowTime > 0) {
            cat = catLeftMeow;
        } else {
            cat = catLeft;
        }

        StringTokenizer st = new StringTokenizer(cat, "\n");
        StringBuilder spaces = new StringBuilder();
        for(int i=0; i<position; i++) spaces.append(" ");
        StringBuilder catWithPos = new StringBuilder();
        while(st.hasMoreTokens()){
            catWithPos.append(spaces.toString() + st.nextToken() + "\n");
        }
        
        System.out.println(catWithPos);
        
        meowTime--;
    }

    public synchronized void playSound(){
        String fileName = "meow" + new Random().nextInt(6) + ".wav";

        new Thread(new Runnable(){
            public void run(){
                try {
                    Clip clip = AudioSystem.getClip();
                    AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                        Main.class.getResourceAsStream(fileName));
                    clip.open(inputStream);
                    clip.start();
                } catch(Exception e){}
            }
        }).start();
    }

    public synchronized void meow(){
        if(meowTime > 0) return;
        playSound();
        meowTime = 4;
        renderCat();
    }
    
    @Override
    public void run(){
        while(isRunning){
            try{Thread.sleep(500);}catch(Exception e){}
            
            if(isRight && position >= xLimit) {
                isRight = false;
                position--;
            } else if(isRight && position < xLimit) {
                position++;
            } else if(!isRight && position <= 0) {
                isRight = true;
                position++;
            } else {
                position--;
            }

            renderCat();
        }
    }
}

public class Main {    
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Cat cat = new Cat();
        Thread thread = new Thread(cat);
        thread.start();
        while(!br.readLine().equals("-1")) {
            cat.meow();
        }
        cat.shutDown();
    }
}